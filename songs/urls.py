from django.urls import path

from songs.views import show_song

urlpatterns = [
    path("songs/<int:id>/", show_song, name="show_song"),
]