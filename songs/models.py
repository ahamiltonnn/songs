from django.db import models

# Create your models here.
class Song(models.Model):
    title = models.CharField(max_length=150)
    album_cover = models.URLField(max_length=10000, null=True)
    artist = models.CharField(max_length=150)
    date_published = models.CharField(max_length=150)
