from django.shortcuts import render, get_object_or_404
from songs.models import Song
# Create your views here.
def show_song(request, id):
    song = get_object_or_404(Song, id=id)
    context = {
        "song_object": song,
    }
    return render(request, "songs/detail.html", context)